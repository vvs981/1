
import UIKit

class MainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var infoLabel:UILabel!
    @IBOutlet weak var priceLabel:UILabel!
    
    @IBOutlet weak var fromeDateLabel:UILabel!
    @IBOutlet weak var toDateLabel:UILabel!
    
    @IBOutlet weak var fromeTimeLabel:UILabel!
    @IBOutlet weak var toTimeLabel:UILabel!
    
    @IBOutlet weak var NumOfVoyage:UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
