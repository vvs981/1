import UIKit
import CoreData
import SwiftyJSON

class MainTableViewController: UITableViewController {
    
    //Mark: - def varS
    
    var info: String = ""
    var fromDate: String = ""
    var fromTime: String = ""
    var toDate: String = ""
    var toTime: String = ""
    
    var price: Int = 0
    var id: Int = 0
    
    var data: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshData()
    }
    func refreshData() {
        do{
            try loadData()
            tableView.reloadData()
        } catch {
            errorMessage()
        }
    }
    
    func loadData() throws {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
            else {return}
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Information")
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.returnsDistinctResults = true
        
        do {
            data = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func fetchData() {
        let apiURL = "http://smartbus.gmoby.org/web/index.php/api/trips?from_date=20150101&to_date=20180301"
        getDataFromAPI(apiURL)
    }
    
    func getDataFromAPI(_ apiURL:String) {
        let path = apiURL
        let url = URL(string: path)
        let session = URLSession.shared // The singleton shared session
        // Data tasks send and receive data using NSData objects.
        let task = session.dataTask(with: url!, completionHandler: { (data:Data?, response:URLResponse?, error:NSError?) -> Void in
                
                if let jsonData = data {
                    let json = JSON(data: jsonData)
                    
                    for items in 0..<json.count {
                        if let id = json[items]["id"].int, let pr = json[items]["price"].int, let fD = json[items]["from_date"].string, let tD = json[items]["to_date"].string, let fT = json[items]["from_time"].string,  let tT = json[items]["to_time"].string, let inf = json[items]["info"].string {
                            
                            self.id = id
                            self.price = pr
                            self.fromDate = fD
                            self.toDate = tD
                            self.fromTime = fT
                            self.toTime = tT
                            self.info = inf
                        }
                        self.insertValuesInDataStore(id: self.id, pr: self.price, fD: self.fromDate, tD: self.toDate, fT: self.fromTime, tT: self.toTime, inf: self.info)
                        
                        // Could add a closure to the main queue(thread), if we access any UIKit classes.
                        DispatchQueue.main.async(execute: { () -> Void in
                            // Any UI Updates would have been here! & Sent Back to Main Queue
                        })
                    }
                }
                if (error != nil) {
                    self.errorMessage()
                }
                } as! (Data?, URLResponse?, Error?) -> Void)
        task.resume()
    }
    
    // MARK: - Save Data Function
    func insertValuesInDataStore(id: Int, pr: Int, fD: String, tD: String, fT: String, tT: String, inf: String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
            else {
                return
        }
        let managedContext = appDelegate.persistentContainer.viewContext

        let entity = NSEntityDescription.entity(forEntityName: "Information", in: managedContext)!
        
        let inf = NSManagedObject(entity: entity, insertInto: managedContext)
        
        inf.setValue(id, forKeyPath: "id")
        inf.setValue(price, forKeyPath: "price")
        inf.setValue(fromDate, forKeyPath: "from_date")
        inf.setValue(toDate, forKeyPath: "to_date")
        inf.setValue(fromTime, forKeyPath: "from_time")
        inf.setValue(toTime, forKeyPath: "to_time")
        inf.setValue(info, forKeyPath: "info")
        do {
            try managedContext.save()
            data.append(inf)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }


    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let inf = data[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MainTableViewCell
        
        cell.NumOfVoyage?.text = String(describing: inf.value(forKeyPath: "id") as? Int)
        cell.priceLabel?.text = String(describing: inf.value(forKeyPath: "price") as? Int)
        cell.infoLabel?.text = inf.value(forKeyPath: "info") as? String
        cell.fromeDateLabel?.text = inf.value(forKeyPath: "from_date") as? String
        cell.fromeTimeLabel?.text = inf.value(forKeyPath: "from_time") as? String
        cell.toTimeLabel?.text = inf.value(forKeyPath: "to_time") as? String
        cell.toDateLabel?.text = inf.value(forKeyPath: "to_date") as? String
        
        return cell
    }
    
    // MARK: - Error Handling
    func errorMessage() {
        let alertController = UIAlertController(title: "Something went wrong!", message: "Please try later in few moments", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        alertController.addAction(okAction)
        present(alertController, animated: true) {
        }
    }
}
